## 功能介绍 
文体社团活动小程序：随着广大老百姓健康和文化意识的不断加强，对于各项文体活动的需求越来越强烈，各种文体社团也逐步兴起，本小程序是一款为各种文体社团提供活动管理和参与的应用程序。它可以帮助社团管理者发布活动信息、收集报名信息、管理活动进程和统计活动数据。同时，用户可以通过小程序浏览社团活动信息、报名参加活动、查看活动进程和反馈意见建议。还可以每天组织各种打卡活动（上传心得和图片）和评比

该小程序的主要功能包括：
1. 活动发布：社团管理者可以在小程序上发布活动信息，包括活动名称、时间、地点、费用、报名截止时间等。
2. 报名管理：用户可以在小程序上浏览活动信息并报名参加活动，社团管理者可以通过小程序收集报名信息并进行管理。
3. 活动进程管理：社团管理者可以在小程序上管理活动进程，包括签到、活动记录、活动照片等。
4. 活动数据统计：小程序可以统计活动数据，包括参与人数、活动评分、反馈意见等。  
5．打卡模块：可以帮助社团成员记录和评比每天的文艺、体育、健身等活动。通过完成一个个小目标帮助社团成员更好地管理自己的时间和活动。

本项目前后端完整代码包括公告通知，社团风采，活动分类与列表，活动报名与评价，打卡项目列表，打卡排行与每日动态，我的活动报名，我的每日打卡；后台打卡项目管理，打卡记录管理与导出，后台活动与报名管理，报名审核与数据导出等功能，活动组织方可以自定义要填写的内容，比如姓名、性别、年龄，身份证、手机号等。采用腾讯提供的小程序云开发解决方案，无须服务器和域名。

 
![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

![输入图片说明](demo/%E6%96%87%E4%BD%93%E7%A4%BE%E5%9B%A2%E6%B4%BB%E5%8A%A8%E5%B0%8F%E7%A8%8B%E5%BA%8F%20(2).jpg)

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，不会 DDOS攻击，节省防火墙费用，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信: 
 
![输入图片说明](demo/4.png)


## 演示 
 ![输入图片说明](demo/%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

## 安装

- 安装手册见源码包里的word文档 




## 截图


![输入图片说明](demo/0%E9%A6%96%E9%A1%B5.png)

![输入图片说明](demo/1%E5%85%AC%E5%91%8A.png)

![输入图片说明](demo/3%E6%B4%BB%E5%8A%A8.png)

![输入图片说明](demo/4%E6%B4%BB%E5%8A%A8.png)
![输入图片说明](demo/5%E6%B4%BB%E5%8A%A8%E5%90%8D%E5%8D%95.png)
![输入图片说明](demo/6%E6%B4%BB%E5%8A%A8%E5%B9%B3%E5%95%8A.png)
 ![输入图片说明](demo/7%E6%89%93%E5%8D%A1.png)
![输入图片说明](demo/8%E6%89%93%E5%8D%A1%E8%AF%A6%E6%83%85.png)
![输入图片说明](demo/9%E6%89%93%E5%8D%A1-%E6%8E%92%E8%A1%8C.png)
![输入图片说明](demo/10%E6%89%93%E5%8D%A1-%E5%8A%A8%E6%80%81.png)
![输入图片说明](demo/11%E6%89%93%E5%8D%A1-%E6%88%91%E7%9A%84.png)
![输入图片说明](demo/12%E6%88%91%E7%9A%84.png)
![输入图片说明](demo/13%E6%88%91%E7%9A%84%E6%89%93%E5%8D%A1.png)
![输入图片说明](demo/14%E6%88%91%E7%9A%84%E6%B4%BB%E5%8A%A8%E6%8A%A5%E5%90%8D.png)
![输入图片说明](demo/15%E6%8A%A5%E5%90%8D%E8%AF%A6%E6%83%85.png)

## 后台管理系统截图 
- 后台超级管理员默认账号:admin，密码123456，请登录后台后及时修改密码和创建普通管理员。
![输入图片说明](demo/80%E5%90%8E%E5%8F%B0.png)
![输入图片说明](demo/81%E7%A4%BE%E5%9B%A2%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/82%E6%B4%BB%E5%8A%A8%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/83%E6%B4%BB%E5%8A%A8%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/84%E6%B4%BB%E5%8A%A8%E5%90%8D%E5%8D%95.png)
![输入图片说明](demo/85%E6%B4%BB%E5%8A%A8%E5%90%8D%E5%8D%95%E4%B8%8B%E8%BD%BD.png)
![输入图片说明](demo/87%E6%B4%BB%E5%8A%A8%E5%90%8D%E5%8D%95-excel.png)
![输入图片说明](demo/88%E5%90%8E%E5%8F%B0%E6%B4%BB%E5%8A%A8%E7%AD%BE%E5%8D%95.png)
![输入图片说明](demo/90%E6%89%93%E5%8D%A1%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/91%E6%89%93%E5%8D%A1%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/92%E6%89%93%E5%8D%A1%E5%8A%A8%E6%80%81.png)